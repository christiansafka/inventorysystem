﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace InventorySystem
{
    class Program
    {

        struct Product
        {
            public string productCode;
            public string name;
            public int avgSales;
        }

        struct Storage
        {
            public string productCode;
            public int inventory;
        }

        static void Main(string[] args)
        {
            
            Product[] products = new Product[3];
            Storage[] storage = new Storage[3];
            
            bool check = ReadToProducts(products);
            bool check2 = ReadToStorage(storage);

            if (check == true && check2 == true)
            {
                Array.Sort<Product>(products, (x, y) => x.name.CompareTo(y.name));

                List<Product> outOfStockProducts = CheckInventory(products, storage);

                Console.WriteLine("Out of stock items: ");
                foreach (Product p in outOfStockProducts)    //testing the output
                {
                    Console.WriteLine(p.name + " ");
                }
            }

            Console.ReadLine();
        }

        static bool ReadToProducts(Product[] products)
        {
            int counter = 0;
            string line;
            try
            {
                StreamReader file = new StreamReader(Environment.CurrentDirectory + "/products.txt");
                while ((line = file.ReadLine()) != null)
                {
                    string[] data = line.Split(' ');
                    products[counter].productCode = data[0];
                    products[counter].name = data[1];
                    products[counter].avgSales = int.Parse(data[2]);
                    counter++;
                }

                file.Close();
                return true;
            }
            catch { Console.WriteLine("File could not be opened");
                return false;
            }
        }

        static bool ReadToStorage(Storage[] storage)
        {
            int counter = 0;
            string line;
            try
            {
                StreamReader file = new StreamReader(Environment.CurrentDirectory + "/storage.txt");
                while ((line = file.ReadLine()) != null)
                {
                    string[] data = line.Split(' ');
                    storage[counter].productCode = data[0];
                    storage[counter].inventory = int.Parse(data[1]);
                    counter++;
                }

                file.Close();
                return true;
            }
            catch { Console.WriteLine("File could not be opened");
                return false;
            }
        }

        static List<Product> CheckInventory(Product[] products, Storage[] storage)
        {
            List<Product> outOfStockProducts = new List<Product>();
            for (int i = 0; i < products.Length - 1; i++)
            {
                int temp = 0;
                for (int d = 0; d < products.Length; d++)
                {
                    if (storage[i].productCode == products[d].productCode)
                    {
                        temp = d;
                        d = products.Length;
                    }
                }

                 if (storage[i].inventory < products[temp].avgSales*3)
                 {
                     outOfStockProducts.Add(products[temp]);
                 }
                
            }

            return outOfStockProducts;

        }

    }


}

